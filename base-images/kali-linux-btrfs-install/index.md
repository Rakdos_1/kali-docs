---
title: BTRFS Install
description:
icon:
date: 2020-05-28
type: archived
weight: 60
author: ["re4son",]
tags: ["",]
keywords: ["",]
og_description:
---

This has been moved to the [installation](https://www.kali.org/docs/installation/) section. To access this doc specifically, please follow [this link](https://www.kali.org/docs/installation/kali-linux-btrfs-install/).